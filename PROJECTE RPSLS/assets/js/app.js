$(document).ready(function () {

    var userPoints = 0;
 
    var computerPoints = 0;
 
 
    var rules = {
 
        "rock": ["scissors", "lizard"],
 
        "paper": ["rock", "spock"],
 
        "scissors": ["paper", "lizard"],
 
        "lizard": ["spock", "paper"],
 
        "spock": ["scissors", "rock"]
 
    };
 
 
    var computerChoices = [
 
        "./images/paper.png",
 
        "./images/rock.png",
 
        "./images/scissors.png",
 
        "./images/lizard.png",
 
        "./images/spock.png"
 
    ];
 
 
    // Click a les imatges
 
    $(".option").on("click", function () {
 
        var userImagePath = $(this).attr("src");
 
        $("#userChoice").attr("src", userImagePath);
 
 
        var userChoice = getImageName(userImagePath);
 
        playRound(userChoice);
 
    });
 
 
    // Click al PLAY
 
    $("#playButton").on("click", function () {
 
        setTimeout(function () {
 
            var randomIndex = Math.floor(Math.random() * computerChoices.length);
 
            var computerImagePath = computerChoices[randomIndex];
 
            $("#computerChoice").attr("src", computerImagePath);
 
 
            var computerChoice = getImageName(computerImagePath);
 
            playRound(computerChoice);
 
        }, 1000); // Esperar 1 segon
 
    });
 
 
    function getImageName(imagePath) {
 
        return imagePath.split("/").pop().split(".")[0];
 
    }
 
 
    function playRound(choice) {
 
        var userChoice = getImageName($("#userChoice").attr("src"));
 
        var resultText = "Results: ";
 
 
        if (userChoice === choice) {
 
            resultText += "It's a tie! :|";
 
        } else if (rules[userChoice].includes(choice)) {
 
            resultText += "You win! :D ";
 
            userPoints++;
 
        } else {
 
            resultText += "You lose! D:";
 
            computerPoints++;
 
        }
 
 
        $("#userPoints").text("Points: " + userPoints);
 
        $("#computerPoints").text("Points: " + computerPoints);
 
 
        $("#resultText").text(resultText);
 
    }
 
 });
