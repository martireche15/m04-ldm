(: Utilitzant la funció “ distinct-values() ”, modifica l'exercici anterior per a que retorne tan sols un nom per a cada plataforma sense repetir-se.:)

for $i in distinct-values(doc("playstore.xml")/playstore/games/game/platform)
return data($i)