(: Utilitza la funció “ count() ” per a retornar la quantitat de jocs que hi han a la base de dades. :)

let $i:=count(doc("playstore.xml")/playstore/games/game)
return($i)