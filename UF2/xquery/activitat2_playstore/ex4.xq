(: Retorna la quantitat de plataformes diferents que hi ha a la base de dades. :)

for $i in count(distinct-values(doc("playstore.xml")/playstore/games/game/platform))
return data($i)