(: Mostra el nom dels jocs que hi ha disponibles per a “ Wii ” (no s'ha de mostrar l'element contenidor “ <plataforma> ”). :)

for $i in doc("playstore.xml")/playstore/games
where $i/game/platform="Wii "
return data($i/game/name)