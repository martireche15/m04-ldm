(:
Mostra el llistat dels jocs per a majors d'edat, s'ha de veure el següent esquema en format XML

<adult_game>
  <name>Hunted: The Demon’s Forge</name>
  <price>49.95</price>
</adult_game>
<adult_game>
  <name>Red Faction: Armageddon</name>
  <price>70.99</price>
</adult_game>
:)

for $game in doc("playstore.xml")/playstore/games/game
where $game/age >= 18
return
  <adult_game>
    {$game/name}
    {$game/price}
  </adult_game>
