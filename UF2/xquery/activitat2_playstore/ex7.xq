(: Mostra un llistat amb el nom i la companyia dels jocs que contenen la cadena “super” en el nom del joc en majúscules o minúscules. :)

for $game in doc("playstore.xml")/playstore/games/game
where contains(lower-case($game/name), "super")
return data(($game/name, $game/company))
  