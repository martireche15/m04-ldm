(: Mostra els noms dels jocs ordenats per preu de manera ascendent. Els inferiors a 50€ han de contenir l'etiqueta <offer> i els superiors o igual a 50€ l'etiqueta <standard>. :)

for $game in doc("playstore.xml")/playstore/games/game
order by $game/name 
return if ($game/price < 50)
then <offer>{$game/name}</offer>
else <standard>{$game/name}</standard>