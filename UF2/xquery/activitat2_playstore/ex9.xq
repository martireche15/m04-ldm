(: Crea una funció “expenses” de manera que, donat un preu, retorna la suma de l'IVA del preu + una taxa de 2.5€ per transport. El valor resultant ha de ser mostrat amb dos decimals. 

Expenses = Price * 0.21 + 2.5

Ajuda: 
The expression fn:round(2.5) returns 3.0.
The expression fn:round(2.4999) returns 2.0.
The expression fn:round(-2.5) returns -2.0.
The expression fn:round(1.125, 2) returns 1.13.

Cada joc s'ha de retornar amb el següent format:

<product>
	<name>Super Mario Bross Wonder</name>
	<price>48.99</price>
	<expenses>12.79</expenses>
</product>
:)

declare function local:expenses($price as xs:decimal?)
as xs:decimal?
{
  let $iva := $price * 0.21
  let $transportTax := 2.5
  return round($iva + $transportTax, 2)
};

for $game in doc("playstore.xml")/playstore/games/game
return 
  <product>
    {$game/name}
    {$game/price}
    <expenses>{local:expenses($game/price)}</expenses>
  </product>


