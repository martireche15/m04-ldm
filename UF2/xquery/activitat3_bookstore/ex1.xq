(: Generar un document html amb un encapçalament que digui “Book list” seguit d'una taula de 3 columnes, en les que es mostri el títol, la editorial i el preu de cada llibre. :)

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Book list</title>
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
  <h1>Book list</h1>
  <table border="2px solid black">
    <thead>
      <tr>
        <th>TITLE</th>
        <th>EDITORIAL</th>
        <th>PRICE</th>
      </tr>
    </thead>
    <tbody>
    {
      for $book in doc("bookstore.xml")/bookstore/book
      return
        <tr>
          <td>
          {
            let $title := $book/title
            return data($title)
          }
          </td>
          <td>
          {
            let $editorial := $book/editorial
            return data($editorial)
          }
          </td>
          <td>
          {
            let $price := $book/price
            return data($price)
          }
          </td>
        </tr>
    }
    </tbody>
  </table>
</body>
</html>