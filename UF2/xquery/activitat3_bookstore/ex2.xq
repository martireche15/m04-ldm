(: Generar el següent document html amb la informació dels llibres del autor amb cognom "Abiteboul". El valor “Total price” es un camp calculat. :)

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Book list</title>
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
  <h1>Book list</h1>
  <table border="2px solid black">
    <thead>
      <tr>
        <th colspan="5">{
          let $lastname := "Stevens"
          return $lastname
        }</th>
      </tr>
      <tr>
        <th>TITLE</th>
        <th>YEAR</th>
        <th>AUTHORS</th>
        <th>EDITORIAL</th>
        <th>PRICE</th>
      </tr>
    </thead>
    <tbody>
    {
      let $lastname := "Stevens"
      for $book in doc("bookstore.xml")/bookstore/book[author/lastname = $lastname] 
      return
      
        <tr>
          <td>
          {
            let $title := $book/title
            return data($title)
          }
          </td>
          <td>
          {
            let $year := $book/@year
            return data($year)
          }
          </td>
          <td>
          {
            for $author in $book/author
            return <p>{data(($author/name, $author/lastname))}</p>
          }
          </td>
          <td>
          {
            let $editorial := $book/editorial
            return data($editorial)
          }
          </td>
          <td>
          {
            let $price := $book/price
            return data($price)
          }
          </td>
        </tr>
    }
    </tbody>
  </table>
  {
  let $lastname := "Stevens"
  let $price := sum(doc("bookstore.xml")/bookstore/book[author/lastname = $lastname]/price)
  return
  <p><strong>Total price: {sum($price)} </strong></p>
  }
</body>
</html>