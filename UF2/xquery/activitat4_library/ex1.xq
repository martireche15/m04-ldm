(: 1: Llistar el títol de tots els llibres. :)
for $book in doc("library.xml")/library/book
return data($book/title)