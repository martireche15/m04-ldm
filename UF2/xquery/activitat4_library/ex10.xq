(: 10: Per a cada llibre, obtenir el títol i el número d'autors, agrupats en un element <llibre>. :)

for $book in doc("library.xml")/library/book
return
  <llibre>
    {'&#10;'}
    {concat('Títol: ', data($book/title))}
    {'&#10;'}
    {concat('Núm. autors: ', count($book/author))}
    {'&#10;'}
  </llibre>