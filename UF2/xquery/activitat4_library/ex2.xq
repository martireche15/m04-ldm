(: 2: Llistar l'any i títol de tots els llibres, ordenats per any. :)

for $book in doc("library.xml")/library/book
order by $book/@year
return data(($book/title, $book/@year))