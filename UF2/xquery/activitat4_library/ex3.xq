(: 3: Llistar els llibres amb preu de 65,95. :)

for $book in doc("library.xml")/library/book
where $book/price=65.95
return data(($book/title, $book/price))