(: 4: Llistar els llibres publicats abans de l'any 2000. :)

for $book in doc("library.xml")/library/book
where $book/@year=2000
return data(($book/title, $book/@year))