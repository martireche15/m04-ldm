(: 5: Llistar any i títol dels llibres publicats per Addison-Wesley després de l'any 1992. :)

for $book in doc("library.xml")/library/book
where $book/@year>1992 and $book/editorial="Addison-Wesley"
return data(($book/title, $book/editorial, $book/@year))