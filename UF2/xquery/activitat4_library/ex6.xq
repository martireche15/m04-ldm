(: 6. Llistar any i títol dels llibres que tenen més d'un autor. :)

for $book in doc("library.xml")/library/book
where count($book/author) > 1
return data(($book/title, $book/@year))