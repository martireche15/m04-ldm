(: 7: Llistar any i títol dels llibres que no tenen autor. :)

for $book in doc("library.xml")/library/book
where not($book/author)
return data(($book/title, $book/@year))