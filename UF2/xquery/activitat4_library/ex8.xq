(: 8: Mostrar els cognoms dels autors que apareixen sense repeticions i ordenats alfabèticament. :)

for $lastname in distinct-values(doc("library.xml")/library/book/author/lastname)
order by $lastname
return $lastname

