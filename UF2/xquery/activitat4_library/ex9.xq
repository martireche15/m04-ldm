(: 9: Per a cada llibre, llistar agrupat en un element <result> el títol i l'autor. :)

for $book in doc("library.xml")/library/book
return 
  <result>
    {data($book/title)}
    {'&#10;'}
    {data($book/author/lastname)}
  </result>