(: 1: Llistar totes les espècies de tots els pokemon. :)

for $pokemon in doc("pokedex.xml")/pokedex/pokemon
return data($pokemon/species)
