(: 2: Llistar tots els pokemons i les habilitats, ordenats per nom. Afegiex també les etiquetes <pokemons> i <pokemon>. :)

<pokemons>
{for $pokemon in doc("pokedex.xml")/pokedex/pokemon
return 
  <pokemon>
    {'&#10;'}
    {<name>{data($pokemon/species)}</name>}
    {'&#10;'}
    {$pokemon/abilities/ability}
    {'&#10;'}
  </pokemon>
}
</pokemons>