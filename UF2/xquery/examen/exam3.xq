(: 3: Aconsegueix el total de punts de cada pokemon ordenat per punts descendentment. Afegiex també les etiquetes <pokemons> i <pokemon>. :)

<pokemons>
{
for $pokemon in doc("pokedex.xml")/pokedex/pokemon
order by sum($pokemon/baseStats/*)
return 
   <pokemon>
    {'&#10;'}
    {$pokemon/species}
    {'&#10;'}
    {<total>{sum($pokemon/baseStats/*)}</total> }
    {'&#10;'}    
  </pokemon>
}
</pokemons>
