(: 4: Seleccionar tots els pokemons que són de tipus FLYING ordenat per nom. Afegiex també les etiquetes <pokemons> i <pokemon>. :)

<pokemons>
{
for $pokemon in doc("pokedex.xml")/pokedex/pokemon[types/type = "FLYING"]
order by $pokemon/species
return
<pokemon>
  {'&#10;'}
  {$pokemon/species}
  {'&#10;'}
  {<types>{$pokemon/types/type}</types>}
  {'&#10;'}
</pokemon>
}
</pokemons>
