(: 5: Mostra els pokemons que tenen l'atac superior a 100. Afegiex també les etiquetes <pokemons> i <pokemon>. :)

<pokemons>
{
for $pokemon in doc("pokedex.xml")/pokedex/pokemon[baseStats/ATK > 100]
order by $pokemon/baseStats/ATK
return 
<pokemon>
  {'&#10;'}
  {$pokemon/species}
  {'&#10;'}
  {$pokemon/baseStats/ATK}
  {'&#10;'}
</pokemon>
}
</pokemons>