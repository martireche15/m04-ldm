(: 6: Genera la següent pàgina web. :)

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Pokemons ATK > 100</title>
  <link rel="stylesheet" href="style.css"/>
</head>
<body>
  <h1>Pokemons</h1>
  <table border="2px solid black">
    <thead>
      <tr>
        <th>Image</th>
        <th>Pokemon</th>
        <th>HP</th>
        <th>ATK</th>
        <th>DEF</th>
        <th>SPD</th>
        <th>SATK</th>
        <th>SDEF</th>
      </tr>
    </thead>
    <tbody>
    {
      for $pokemon in doc("pokedex.xml")/pokedex/pokemon[baseStats/ATK > 100]
      order by $pokemon/baseStats/ATK
      return
        <tr>
          <td>
          {
            let $image := $pokemon/image
            return
            <img src="{ data($image)}" width="20%"></img>
          }
          </td>
          <td>{$pokemon/species}</td>
          <td>{$pokemon/baseStats/HP}</td>
          <td>{$pokemon/baseStats/ATK}</td>
          <td>{$pokemon/baseStats/DEF}</td>
          <td>{$pokemon/baseStats/SPD}</td>
          <td>{$pokemon/baseStats/SATK}</td>
          <td>{$pokemon/baseStats/SDEF}</td>
        </tr>
    }
    </tbody>
  </table>
</body>
</html>