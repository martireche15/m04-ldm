<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>XSL Transform</title>
            <meta charset="UTF-8" />
            <link rel="stylesheet" href="activitat1.css" />
        </head>
        <body>
            <h1>Film List</h1>
            <table border="1">
                <tr>
                    <th>Title</th>
                    <th>Language</th>
                    <th>Year</th>
                    <th>Country</th>
                    <th>Genre</th>
                    <th>Summary</th>
                </tr>
                <xsl:for-each select="//film">
                    <tr>
                        <td>
                            <xsl:value-of select="title" />
                        </td>
                        <td>
                            <xsl:value-of select="title/@lang" />
                        </td>
                        <td>
                            <xsl:value-of select="year" />
                        </td>
                        <td>
                            <xsl:value-of select="country" />
                        </td>
                        <td>
                            <xsl:value-of select="genre" />
                        </td>
                        <td>
                            <xsl:value-of select="summary" />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>