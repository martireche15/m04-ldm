<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>ACTIVITAT 2: coding</title>
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <table>
                    <thead>
                        <tr>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>License</th>
                        </tr>
                    </thead>
               
                <tbody>
                    <xsl:for-each select="coding/programs/program">
                        <tr>
                            <td class="logo">
                                <xsl:variable name="imageSrc" select="logo" />
                                <img src="{$imageSrc}" style="height:75px ; width:75px;"/>
                            </td>
                            <td><xsl:value-of select="name" /></td>
                            <td><xsl:value-of select="type" /></td>
                            <td><xsl:value-of select="license" /></td>
                        </tr>
                    </xsl:for-each>
                </tbody>
                </table>
            </body>
        </html>

    </xsl:template>
</xsl:stylesheet>
