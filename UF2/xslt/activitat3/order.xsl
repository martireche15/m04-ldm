<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>ACTIVITAT 3: order</title>
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <h4>Products with a Price &gt; 24 and Price &lt;= 100</h4>
                    <div class="table1">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">CUSTOMER DETAILS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="order/destination">
                            <tr>
                                <td>Name</td>
                                <td><xsl:value-of select="name" /></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td class="grey"><xsl:value-of select="address" /></td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td><xsl:value-of select="city" /></td>
                            </tr>
                            <tr>
                                <td>P.C.</td>
                                <td class="grey"><xsl:value-of select="postalcode" /></td>
                            </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                    </div>
                    <br/>
                    <div class="table2">
                    <table>
                        <thead>
                            <tr>
                                <th class="blue" colspan="4" >ORDER</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Product</td>
                                <td>Price</td>
                                <td>Quantity</td>
                                <td>Total</td>
                            </tr>
                        
                            <xsl:for-each select="order/products/product[price > 25 and price <= 100]" >
                            <xsl:sort select="price" order="ascending" data-type="number"/>
                            <tr class="{concat('yellow ', 
                                        if (price > 25 and price < 50) then 'yellow'
                                        else if (price >= 50 and price < 75) then 'green'
                                        else if (price >= 75 and price <= 100) then 'red'
                                        else '')}">
                                <td >

                                <xsl:value-of select="name" /> 
                                (code = <xsl:value-of select="@code"/>)
                                </td>
                                <td><xsl:value-of select="price" /></td>
                                <td><xsl:value-of select="quantity" /></td>
                                <td> 
                                    <xsl:variable name="price">
                                        <xsl:value-of select="price"/> 
                                    </xsl:variable>
                                    <xsl:variable name="quantity">
                                        <xsl:value-of select="quantity"/> 
                                    </xsl:variable> 
                                    <xsl:value-of select="$price * $quantity"/>
                                </td>
                            </tr>
                            </xsl:for-each>
                        </tbody>    
                    </table>
                    </div>
            </body>
        </html>

    </xsl:template>
</xsl:stylesheet>