<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="book" >
        Book: <xsl:value-of select="title"/>(<xsl:value-of select="year"/>)<br/>
        Category: <xsl:value-of select="@category"/><br/>
        
        <xsl:for-each select=".">
            <xsl:value-of select="title"/><br/>
            <xsl:value-of select="author"/><br/>
        </xsl:for-each>

    </xsl:template>
</xsl:stylesheet>

