<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/" >
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>XSL Transform - Attribute</title>
            <meta charset="UTF-8" />
        </head>
        <body>
            <h2>My CD Collection - Language</h2>
            <table border="1">
                <tr bgcolor="#9acd32">
                    <th>Title</th>
                    <th>Language</th>
                </tr>
                <xsl:for-each select="/bookstore/book">
                    <tr>
                        <td>
                            <xsl:value-of select="title" />
                        </td>
                        <td>
                            <xsl:value-of select="title/@lang" />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>