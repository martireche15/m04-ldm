<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>examen uefa</title>
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
            <div>
                <xsl:variable name="logouefa" select="UEFA/season/organization/logo_uefa" />
                <img src="{$logouefa}" style="height:75px ; width:75px;"/>
            </div>
                <h1>UEFA Champions League</h1>
                <table>
                    <thead>
                        <tr>
                            <th colspan="7">Quarter finals</th>
                            <th>Winner</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="UEFA/season/quarter-finals/match">
                            <tr>
                                <td>
                                    <xsl:variable name="logo1" select="first-leg/local/logo" />
                                    <img src="{$logo1}" style="height:75px ; width:75px;"/>
                                </td>
                                <td>
                                    <xsl:value-of select="first-leg/local/name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="first-leg/local/goals"/>
                                </td>
                                <td><h2>-</h2></td>
                                <td>
                                    <xsl:value-of select="first-leg/visitant/goals"/>
                                </td>
                                <td>
                                    <xsl:value-of select="first-leg/visitant/name"/>
                                </td>
                                <td>
                                    <xsl:variable name="logo2" select="first-leg/visitant/logo" />
                                    <img src="{$logo2}" style="height:75px ; width:75px;"/>
                                </td>
                                <td rowspan="2">
                                    <xsl:variable name="winnername" select="winner"/>
                                    <xsl:choose> 
                                        <xsl:when test="first-leg/local/id=$winnername">
                                            <xsl:variable name="winnerlogo" select="first-leg/local/logo" />
                                            <img src="{$winnerlogo}" style="height:75px ; width:75px;"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:variable name="winnerlogo" select="first-leg/visitant/logo" />
                                            <img src="{$winnerlogo}" style="height:75px ; width:75px;"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <xsl:variable name="logo1" select="second-leg/local/logo" />
                                    <img src="{$logo1}" style="height:75px ; width:75px;"/>
                                </td>
                                <td>
                                    <xsl:value-of select="second-leg/local/name"/>
                                </td>
                                <td>
                                    <xsl:value-of select="second-leg/local/goals"/>
                                </td>
                                <td><h2>-</h2></td>
                                  <td>
                                    <xsl:value-of select="second-leg/visitant/goals"/>
                                </td>
                                <td>
                                    <xsl:value-of select="second-leg/visitant/name"/>
                                </td>
                                <td>
                                    <xsl:variable name="logo2" select="second-leg/visitant/logo" />
                                    <img src="{$logo2}" style="height:75px ; width:75px;"/>
                                </td>

                            </tr>
                        </xsl:for-each>
                        

                    </tbody>
                </table>
                <p>Total goals: <xsl:value-of select="sum(//goals)" /></p>
                <p>Number of matches: <xsl:value-of select="count(//match) * 2" /></p>
                <p>Average goals: <xsl:value-of select="round(sum(//goals) div count(//match)) div 2" /></p>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
