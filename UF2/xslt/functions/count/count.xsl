<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
            </head>
            <body>
                <h2>Bookstore count</h2>
                <xsl:for-each select="bookstore">
                    <p>Total books: <xsl:value-of select="count(//book)" /></p>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>