<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Variables</title>
                <link rel="stylesheet" href="style.css" />
            </head>
            <body>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Salary</th>
                            <th>Bonus</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="company/employees/employee">
                            <tr>
                                <xsl:choose>
                                    <xsl:when test="position() = 1">
                                        <td bgcolor="yellow"><b><xsl:value-of select="id" /></b></td>
                                    </xsl:when>
                                    <xsl:when test="position() = last()">
                                        <td bgcolor="red"><b><xsl:value-of select="id" /></b></td>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <td bgcolor="blue"><b><xsl:value-of select="id" /></b></td>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <td><b><xsl:value-of select="name" /></b></td>
                                <td><xsl:value-of select="email" /></td>
                                <td><xsl:value-of select="salary" /></td>
                                <td><xsl:value-of select="bonus" /></td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
